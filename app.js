const http = require('http');
const fs = require('fs');

const PORT = 8000;

// HTML
const onRequest = (req, res) => {
  console.log(req.url, res.method);

  let PATH = './views/';

  switch (req.url) {
    case '/':
      PATH += 'index.html';
      res.statusCode = 200;
      break;
    case '/about':
      PATH += 'about.html';
      res.statusCode = 200;
      break;
    case '/about-me':
      res.statusCode = 301;
      res.setHeader('Location','/about');
      return res.end();
    case '/contact':
      PATH += 'contact.html';
      res.statusCode = 200;
      break;
    default:
      PATH += '404.html';
      res.statusCode = 404;
  }
  
  // Set header content type
  res.setHeader("Content-Type","text/html");
  
  // Send an HTML file
  fs.readFile(PATH, null, (err, data) => {
    if (err) {
      res.writeHead(404);
      res.write("File Not Found");
    } else {
      res.write(data);
    }
    res.end();
  })
}

// Server disiapkan + Routing
const server = http.createServer(onRequest);

// Menjalankan server
server.listen(PORT, 'localhost', () => {
  console.log(`Listening for request on port ${PORT} (http://localhost:${PORT}/)`)
});
