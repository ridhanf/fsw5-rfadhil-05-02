## Binar FSW5 Chapter 05 Session 02 - HTTP Server
This repository contains a simple Routing App with the implementation of HTTP Server using Node.js. This repository was created to complete the task of Chapter 5 Session 2 from Binar Academy. The main source code is on [app.js](./app.js) and you can check [package.json](./package.json) for dependencies.

### License
All source code in this repository is available jointly under the MIT License. See [LICENSE](LICENSE) for details.

### How to use
Feel free to clone and code it by yourself to learn more about HTTP Server.

Follow this bash script to start the HTTP server:
```sh
git clone https://gitlab.com/ridhanf/fsw5-rfadhil-05-02.git
npm install
npm run start
```

Or follow this bash script to start the HTTP server with Nodemon:
```sh
git clone https://gitlab.com/ridhanf/fsw5-rfadhil-05-02.git
npm install
npm run dev
```

That's all. Thank you.

&nbsp;

Sincerely,

Ridhan Fadhilah