const http = require('http');
const fs = require('fs');

const PORT = 8000;

// JSON
const onRequest = (req, res) => {
  res.writeHead(200, {"Content-Type":"application/json"});
  fs.readFile("./masterdata.json", null, (err, data) => {
    if (err) {
      res.writeHead(404);
      res.write("File Not Found");
    } else {
      res.write(data);
    }
    res.end();
  })
}

http.createServer(onRequest).listen(PORT, 'localhost', () => {
  console.log('Listening for request on port 8000')
});
